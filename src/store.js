import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

const apiUrl = 'https://rest.vmeste-region.ru/api/votes';
const store = new Vuex.Store({
  state: {
    votesCards: [],
    pagination: {},
    lock: false
  },
  mutations: {
    setVotes(state, payload) {
      state.votesCards = state.votesCards.concat(payload.votes);
      state.pagination = { ...payload.pagination};
    },
    toggleLock(state, set) {
      state.lock = set;
    }
  },
  actions: {
    fetchData() {
      if (store.state.lock) {
        return false;
      }
      let params = {
        perPage: 6,
        expired: true,
      };
      let pagination = store.state.pagination;
      pagination.hasMorePages ? params.page = pagination.currentPage + 1 : '';
      store.commit('toggleLock', true);
      axios.get(apiUrl, {params})
        .then((response) => {
          store.commit('toggleLock', false);
          store.commit('setVotes', response.data.data);
        })
        .catch((error => {
          store.commit('toggleLock', false);
          console.log(error)
        }))
    }
  },
});

export default store;